This is the project for UrbanWork(So called as of now)

Project is built on node.js with objection.js, knex,(more to come...)

Folder structure
uw
  app.js - launch point for the app
  knexfile.js - file contains the code for knex connection creation
  node_modules - node libraries
  migrations - folder created by knex for migrations
  controllers - folder contains all rest api handlers
  models - folder contains all objection.js modules
  helpers - folder contains all helpers or services
  config.js - file contains all configurations for the app
  README.md
  TODO.md



Home Page Search Results :
--------------------------

/task/search/fetchAllByFilters -- Employer,Location,type,id,range

/task/search/fetchRecommendedJobs
/task/search/fetchPopularJobs


Task Creation and update :
---------------------------
/task/template/form  -- PUT,POST,GET,DELETE
/task/template/list
/task/template/recommendations
/task/template/preferences

/task/instance/form  -- POST,GET, PUT
/task/instance/applications
/task/instance/contacts
