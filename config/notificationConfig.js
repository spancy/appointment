var defaults = {
  COMPLETE_KYC_EMPLOYEE: {
    title: function() {
      return "Complete your KYC";
    },

    message: function() {
      return "Complete your KYC";
    }
  },
  COMPLETE_KYC_EMPLOYER: {
    title: function() {
      return "Complete your KYC";
    },

    message: function() {
      return "Complete your KYC";
    }
  },
  APPLIED_FOR_TASK: {
    title: function() {
      return `New Application for task ${this.taskName}[${this.taskID}]`;
    },

    message: function() {
      return `User ${this.employeeName} has applied for the task ${
        this.taskName
      }[${this.taskID}]. Kindly review and confirm accordingly.`;
    }
  },
  CONFIRMED_FOR_TASK: {
    title: function() {
      return `Task confirmation for ${this.taskName}[${this.taskID}]`;
    },

    message: function() {
      return `Employer ${
        this.employerName
      } has confirmed your application for the task ${this.taskName}[${
        this.taskID
      }]`;
    }
  },
  CANCELLED_APPLICATION: {
    title: function() {
      return `Task application cancelled for ${this.taskName}[${this.taskID}]`;
    },

    message: function() {
      return `Employee ${
        this.employerName
      } has cancelled application for the task ${this.taskName}[${this.taskID}`;
    }
  },
  REQUEST_FOR_START_TASK: {
    title: function() {
      return `Task Start Request for ${this.taskName}[${this.taskID}]`;
    },

    message: function() {
      return `Employee ${
        this.employeeName
      } has arrived at the location and has requested for starting the task ${
        this.taskName
      }[${this.taskID}`;
    }
  },
  ACCEPT_START_TASK: {
    title: function() {
      return `Task Start Request accepted for ${this.taskName}[${this.taskID}]`;
    },

    message: function() {
      return `Employer ${
        this.employerName
      } has accepted your request for starting the task ${this.taskName}[${
        this.taskID
      }`;
    }
  },
  REJECT_START_TASK: {
    title: function() {
      return `Task Start Request rejected for ${this.taskName}[${this.taskID}]`;
    },

    message: function() {
      return `Employer ${
        this.employerName
      } has rejected your request for starting the task ${this.taskName}[${
        this.taskID
      }] Kindly arrive at location and request again`;
    }
  },
  REQUEST_FOR_END_TASK: { title: "Complete your KYC" },
  ACCEPT_END_TASK: { title: "Complete your KYC" },
  REJECT_END_TASK: { title: "Complete your KYC" },
  EMPLOYER_PAYMENT_SUCCESS: { title: "Complete your KYC" },
  EMPLOYER_PAYMENT_FAILURE: { title: "Complete your KYC" },
  EMPLOYEE_PAYMENT_SUCCESS: { title: "Complete your KYC" },
  EMPLOYEE_PAYMENT_FAILURE: { title: "Complete your KYC" }
};

module.exports = defaults;
