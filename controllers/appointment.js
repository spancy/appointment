var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/appointmentPostHelper");
var getHelper = require("../helpers/appointmentGetHelper");
var putHelper = require("../helpers/appointmentPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new instance is added
  .post(async function(req, res) {
    let response = await postHelper.appointmentInsert(req);
    res.json(response);
  });



router
  .route("/all")
  // fetch all instance data
  .get(async function(req, res) {
    let response = await getHelper.appointmentGetAllData(req);
    if (req.query && req.query.lon && req.query.lat) {
      console.log(req.query.lon);
      console.log(req.query.lat);
      response.body = response.body.filter(function(el) {
        return (
          el.lat !== null &&
          el.lon !== null &&
          getHelper.calcCrow(req.query.lat, req.query.lon, el.lat, el.lon) < 30
        );
      });
    }
    res.json(response);
  });

  router
    .route("/all/:offset")
    // fetch all instance data
    .get(async function(req, res) {
      let response = await getHelper.appointmentGetAllDataOffset(req,"offset");
      if (req.query && req.query.lon && req.query.lat) {
        console.log(req.query.lon);
        console.log(req.query.lat);
        response.body = response.body.filter(function(el) {
          return (
            el.lat !== null &&
            el.lon !== null &&
            getHelper.calcCrow(req.query.lat, req.query.lon, el.lat, el.lon) < 30
          );
        });
      }
      res.json(response);
    });

router
  .route("/getByID/:id")
  // fetch a single instance data
  .get(async function(req, res) {
    let response = await getHelper.appointmentGetData(req, "id");

    res.json(response);
  });

  router
    .route("/getCompletedAppointmentsBySalon/:salonID")
    // fetch a single instance data
    .get(async function(req, res) {
      let response = await getHelper.appointmentCompletedGetDataOffset(req, "salonID");

      res.json(response);
    });

    router
    .route("/getCompletedAppointmentsByStatus/:salonID/:appointmentStatus")
    // fetch a single instance data
    .get(async function(req, res) {
      let response = await getHelper.appointmentCompletedGetDataStatusOffset(req, "salonID","appointmentStatus");

      res.json(response);
    });

    router
    .route("/getCompletedTasksByCustomer/:userID")
    // fetch a single instance data
    .get(async function(req, res) {
      let response = await getHelper.appointmentCompletedGetDataOffset(req, "userID");

      res.json(response);
    });



router
  .route("/getSalonID/:salonID")
  // fetch instances for a particular template
  .get(async function(req, res) {
    let response = await getHelper.appointmentGetData(req, "salonID");
    if (req.query && req.query.lon && req.query.lat) {
      console.log(req.query.lon);
      console.log(req.query.lat);
      response.body = response.body.filter(function(el) {
        return (
          el.lat !== null &&
          el.lon !== null &&
          getHelper.calcCrow(req.query.lat, req.query.lon, el.lat, el.lon) < 30
        );
      });
    }
    res.json(response);
  });

router
  .route("/getByCustomerID/:userID")
  // fetch instances for a particular template
  .get(async function(req, res) {
    let response = await getHelper.appointmentGetData(req, "userID");

    res.json(response);
  });

  router
  .route("/getByDate/:salonId")
  // fetch instances for a particular template
  .get(async function(req, res) {
    let response = await getHelper.appointmentGetDataByDate(req, "salonID");

    res.json(response);
  });

  router
    .route("/getCostsBySalonID/:salonID")
    // fetch instances for a particular template
    .get(async function(req, res) {
      let response = await getHelper.appointmentGetTotalEarnings(req, "salonID");

      res.json(response);
    });

    router
    .route("/getCostsByProfessionalID/:professionalID")
    // fetch instances for a particular template
    .get(async function(req, res) {
      let response = await getHelper.appointmentGetTotalEarnings(req, "professionalID");

      res.json(response);
    });


    router
    .route("/updateBySalon/:salonID")
  
    //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
    .put(async function(req, res) {
      let response = await putHelper.appointmentUpdate(req, "salonID");
      res.json(response);
    });

    router
    .route("/updateByCustomer/:userID")
  
    //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
    .put(async function(req, res) {
      let response = await putHelper.appointmentUpdate(req, "userID");
      res.json(response);
    });

    router
    .route("/updateByID/:id")
  
    //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
    .put(async function(req, res) {
      let response = await putHelper.appointmentUpdate(req, "id");
      res.json(response);
    });

module.exports = router;
