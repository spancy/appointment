var { QueryBuilder } = require("objection");
var appointment = require("../models/Appointment");

var integrations = require("./integrations");
const stringify = require('json-stringify');
const moment = require('moment');
//function to get all templates and its underlying relations

//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
function calcCrow(lat1, lon1, lat2, lon2) {
  var R = 6371; // km
  var dLat = toRad(lat2 - lat1);
  var dLon = toRad(lon2 - lon1);
  var lat1 = toRad(lat1);
  var lat2 = toRad(lat2);

  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
}

// Converts numeric degrees to radians
function toRad(Value) {
  return (Value * Math.PI) / 180;
}

//function to get all appointment and its underlying relations
async function appointmentGetAllData(req) {
  try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";
    let eagerLoadedString = "";
    //1 for advocates
    switch (userType) {
      case "1":
        eagerLoadedString =
          "[appointment_status]";
        break;
      default:
        eagerLoadedString =
          "[appointment_status]";
        break;
    }

    const appointmentAllData = await appointment.query().
                        //limit(10).
                        //offset(0).
                        eager(eagerLoadedString);

  //  console.log("count is "+taskAllData.length);

    let message = "";

    message = {
      statusCode: 200,
      body: appointmentAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function appointmentGetAllDataOffset(req,offset) {
  try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";
    let eagerLoadedString = "";
    //1 for advocates
    switch (userType) {
      case "1":
      eagerLoadedString =
        "[appointment_status]";
      break;
    default:
      eagerLoadedString =
        "[appointment_status]";
      break;
    }
    //console.log("Offset is "+req.params.offset);
    const appointmentAllData = await appointment.query().
                        whereIn('appointmentStatus', ['1','2','3']).
                        where('scheduledDate','>',moment()).
                        orderBy('scheduledDate','asc').
                        limit(10).
                        offset(req.params.offset).
                        eager(eagerLoadedString);

  //  console.log("count is "+taskAllData.length);

    let message = "";

    message = {
      statusCode: 200,
      body: appointmentAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all appointment and its underlying relations
async function appointmentGetData(req, queryBy) {
  try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";

    let eagerLoadedString = "";
    
    switch (userType) {
      case "1":
      eagerLoadedString =
        "[appointment_status]";
      break;
    default:
      eagerLoadedString =
        "[appointment_status]";
      break;
    }

    const taskAllData = await appointment
      .query()
      .where(queryBy, req.params[queryBy])
      .orderBy('scheduledDate','Desc')
      .eager(eagerLoadedString);

    let message = "";
console.log("Appointment data ---> "+
JSON.stringify
(taskAllData.body));
    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}


//By status
async function appointmentCompletedGetDataStatusOffset(req, salonID, appointmentStatus) {
  try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";

    let eagerLoadedString = "";
    
    switch (userType) {
      case "1":
      eagerLoadedString =
        "[appointment_status]";
      break;
    default:
      eagerLoadedString =
        "[appointment_status]";
      break;
    }

    const taskAllData = await appointment
      .query()
      .where(salonID, req.params[salonID])
      .where(appointmentStatus, req.params[appointmentStatus])
      .orderBy('scheduledDate','Desc')
      .eager(eagerLoadedString);

    let message = "";
console.log("Appointment data ---> "+
JSON.stringify
(taskAllData.body));
    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}


//Get appointments by date and salon_id

async function appointmentGetDataByDate(req, queryBy) {
  try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";

    let eagerLoadedString = "";
    
    switch (userType) {
      case "1":
      eagerLoadedString =
        "[appointment_status]";
      break;
    default:
      eagerLoadedString =
        "[appointment_status]";
      break;
    }

    const taskAllData = await appointment
      .query()
      .where(queryBy, req.params[queryBy])
      .where("scheduled_date", req.query.date)
      .orderBy('scheduledDate','Desc')
      .eager(eagerLoadedString);

    let message = "";
console.log("Appointment data ---> "+
JSON.stringify
(taskAllData.body));
    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function appointmentGetDataCount(req, queryBy) {
  /*try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";

    let eagerLoadedString = "";
    //1 for advocates
    switch (userType) {
      case "1":
        eagerLoadedString =
          "[taskApplications.application_status,taskContacts,task_status,advocateTasks]";
        break;
      default:
        eagerLoadedString =
          "[taskApplications.application_status,taskContacts,task_status]";
        break;
    }

    const taskAllData = await taskappointment
      .query()
      .where(queryBy, req.params.id)
      .orderBy('taskScheduledDate','Desc')
      .eager(eagerLoadedString);

    let message = "";
console.log("Task appointment taskdata ---> "+
JSON.stringify
(taskAllData.body));
    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }*/
}

async function appointmentCompletedGetDataOffset(req,queryBy) {
  try {
    //let userEmail = req.apiGateway.event.requestContext.authorizer.claims.email;
    //let userType = await integrations.getUserTypeByEmail(userEmail);
    let userType = "1";
    let eagerLoadedString = "";
    //1 for advocates
    switch (userType) {
      case "1":
      eagerLoadedString =
        "[appointment_status]";
      break;
    default:
      eagerLoadedString =
        "[appointment_status]";
      break;
    }
    //console.log("Offset is "+req.params.offset);
    const taskAllData = await appointment.query().
                        where(queryBy, req.params[queryBy]).
                        where('appointmentStatus', 5).
                        orderBy('scheduledDate','Desc').
                        limit(3).
                        offset(0).
                        eager(eagerLoadedString);

  //  console.log("count is "+taskAllData.length);

    let message = "";

    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function appointmentGetTotalEarnings(req,queryBy) {
  try {


    //console.log("Offset is "+req.params.offset);
    const taskAllData = await appointment.query().
                        where(queryBy, req.params[queryBy]).
                        where('appointmentStatus', 5).
                        count('appointmentStatus').
                        sum('cost');

  //  console.log("count is "+taskAllData.length);

    let message = "";

    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function appointmentGetNumberCreated(req,queryBy) {
  try {


    //console.log("Offset is "+req.params.offset);
    const taskAllData = await appointment.query().
                        where(queryBy, req.params[queryBy]).
                        where('appointmentStatus', 5).
                        count('appointmentStatus')


  //  console.log("count is "+taskAllData.length);

    let message = "";

    message = {
      statusCode: 200,
      body: taskAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}


module.exports.appointmentGetData = appointmentGetData;
module.exports.appointmentGetTotalEarnings = appointmentGetTotalEarnings;
module.exports.appointmentGetNumberCreated = appointmentGetNumberCreated;
module.exports.appointmentGetAllDataOffset = appointmentGetAllDataOffset;
module.exports.appointmentGetAllData = appointmentGetAllData;
module.exports.appointmentCompletedGetDataOffset = appointmentCompletedGetDataOffset;
module.exports.calcCrow = calcCrow;
module.exports.appointmentGetDataByDate = appointmentGetDataByDate;
module.exports.appointmentCompletedGetDataStatusOffset = appointmentCompletedGetDataStatusOffset;


