var { QueryBuilder } = require("objection");
var appointment = require("../models/Appointment");



//function to insert into appointment table and relations.
async function appointmentInsert(req) {
  const appointmentData = {
    salonID: req.body.salonID,
    userID: req.body.userID,
    salonServiceID: req.body.salonServiceID,
    serviceID: req.body.serviceID,
    professionalID: req.body.professionalID,
    bookingLocation: req.body.bookingLocation,
    salonType: req.body.salonType,
    location: req.body.location,
    duration: req.body.duration,
    cost: req.body.cost,
    currency: req.body.currency,
    appointmentLocation: req.body.appointmentLocation,
    appointmentLocationShort: req.body.appointmentLocationShort,
    requirementDescription: req.body.requirementDescription,
    lon: req.body.lon,
    lat: req.body.lat,
    customerPhoneContact: req.body.customerPhoneContact,
    secondaryDisplayImageIDs: req.body.secondaryDisplayImageIDs,
    scheduledDate: req.body.scheduledDate,
    startTime: req.body.startTime,
    endTime: req.body.endTime,
    customerName: req.body.customerName,
    isFeePaid: req.body.isFeePaid,
    isFeePaidOnline: req.body.isFeePaidOnline,
    tipsAmountPaid: req.body.tipsAmountPaid,
    totalAmountPaid: req.body.totalAmountPaid,
    appointmentStatus: req.body.appointmentStatus,
    creditsSpent: req.body.creditsSpent,
    bookedSlotID: req.body.bookedSlotID
  };

  try {
    let message = "";
    const appointmentInstance = await appointment.query().insertGraph(appointmentData);

    message = {
      statusCode: 200,
      body: {
        message: "New Appointment Created",
        id: appointmentInstance.id
      }
    };

    console.log(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Appointment Errored" + e
    };
    console.log(message);
    return message;
  }
}

module.exports.appointmentInsert = appointmentInsert;
