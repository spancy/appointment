var appointments = require("../models/Appointment");

//function to insert into template table and relations.

//function to insert into instance table and relations.
async function appointmentUpdate(req, queryBy) {
  try {
    let message = "";
    const instance = await appointments
      .query()
      .update(req.body)
      .where(queryBy, req.params[queryBy]);

    message = {
      statusCode: 200,
      body: {
        message: "Appointment Updated",
        id: instance.id
      }
    };

    console.log(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Appointments update Errored" + e
    };
    console.log(message);
    return message;
  }
}



module.exports.appointmentUpdate = appointmentUpdate;
