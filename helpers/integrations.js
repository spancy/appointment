var hostname =
  "https://fn0if8ahs3.execute-api.ap-south-1.amazonaws.com/default";

async function forwardRequest(method, path, payload) {
  try {
    var options = {
      method: method,
      url: hostname + path,
      data: payload
    };
    console.log(options);
    const response = await axios(options);
    console.log(response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}

async function getUserTypeByEmail(email) {
  try {
    var response = await forwardRequest(
      "GET",
      "/employee/getUserTypeByEmail/" + email,
      null
    );
    return response.employee.userRole;
  } catch (err) {
    return err;
  }
}
