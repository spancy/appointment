exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema
        .createTable("appointment_status", function(table) {
          table.increments("id").primary();
          table.integer("code");
          table.string("name");
        })
        .then(function() {
          return knex("appointment_status").insert([
            { code: "1", name: "Canceled_by_salon" }, 
            { code: "2", name: "Applied" }, // When booked without payment
            { code: "3", name: "Upcoming" }, // When payment is completed
            { code: "4", name: "Ongoing" },
            { code: "5", name: "Completed" },
            { code: "6", name: "Cancelled" },
            {code: "7", name: "Refunded"},
            {code:"8", name:"No Show"}
          ]);
        }),
        
        knex.schema.createTable("appointment", function(table) {
        table.increments("id").primary();
        table.integer("salonID").notNull();
        table.integer("userID").notNull();
        table.integer("salonServiceID");
        table.integer("serviceID");
        table.integer("professionalID");
        table.string("bookingLocation");
        table.string("salonType");
        table.boolean("location")
              .notNullable()
              .defaultTo(true); //yes-salon no-@home 
        table.integer("duration");
        table.integer("cost");
        table.string("currency");
        table.string("appointmentLocation"); //address
        table.string("appointmentLocationShort"); //area
        table.string("requirementDescription");
        table.decimal("lon");
        table.decimal("lat");
        table.string("customerPhoneContact");
        table.json("secondaryDisplayImageIDs");
        table.timestamp("scheduledDate");
        table.bigInteger("startTime");
        table.bigInteger("endTime");
        table.string("customerName");
        table.boolean("isFeePaid")
              .notNullable()
              .defaultTo(true); 
        table.boolean("isFeePaidOnline")
              .notNullable()
              .defaultTo(true); //yes-Online no-Offline
        table.integer("tipsAmountPaid");
        table.integer("totalAmountPaid");
        table.timestamps(true, true);
        table.integer("appointmentStatus").references("appointment_status.id");
        table.integer("creditsSpent");
      })
  
    ]);
  };
  
  exports.down = function(knex, Promise) {
    return Promise.all([
      knex.schema.dropTableIfExists("appointment")
    ]);
  };
