"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class AppointmentStatus extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "appointment_status";
  }
}

module.exports = AppointmentStatus;
